import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String haystack =
      "CellInfoLte:{mRegistered=YES mTimeStamp=244194682094009ns mCellConnectionStatus=0 CellIdentityLte:{ mBandwidth=20000 mMcc=606 mMnc=00 mAlphaLong=Libyana mAlphaShort=Libyana} CellSignalStrengthLte: rssi=-73 rsrp=-107 rsrq=-13 rssnr=2147483647 cqi=2147483647 ta=3 miuiLevel=3 level=1 android.telephony.CellConfigLte :{ isEndcAvailable = false }}, CellInfoLte:{mRegistered=NO mTimeStamp=244194682094009ns mCellConnectionStatus=0 CellIdentityLte:{ mBandwidth=2147483647 mMcc=null mMnc=null mAlphaLong= mAlphaShort=} CellSignalStrengthLte: rssi=-83 rsrp=-118 rsrq=-20 rssnr=2147483647 cqi=2147483647 ta=2147483647 miuiLevel=1 level=0 android.telephony.CellConfigLte :{ isEndcAvailable = false }}, CellInfoLte:{mRegistered=NO mTimeStamp=244194682094009ns mCellConnectionStatus=0 CellIdentityLte:{ mBandwidth=2147483647 mMcc=null mMnc=null mAlphaLong= mAlphaShort=} CellSignalStrengthLte: rssi=-83 rsrp=-115 rsrq=-20 rssnr=2147483647 cqi=2147483647 ta=2147483647 miuiLevel=2 level=1 android.telephony.CellConfigLte :{ isEndcAvailable = false }}, CellInfoLte:{mRegistered=NO mTimeStamp=244194682094009ns mCellConnectionStatus=0 CellIdentityLte:{ mBandwidth=2147483647 mMcc=null mMnc=null mAlphaLong= mAlphaShort=} CellSignalStrengthLte: rssi=-83 rsrp=-108 rsrq=-12 rssnr=2147483647 cqi=2147483647 ta=2147483647 miuiLevel=3 level=1 android.telephony.CellConfigLte :{ isEndcAvailable = false }}, CellInfoLte:{mRegistered=NO mTimeStamp=244194682094009ns mCellConnectionStatus=0 CellIdentityLte:{ mBandwidth=2147483647 mMcc=null mMnc=null mAlphaLong= mAlphaShort=} CellSignalStrengthLte: rssi=-83 rsrp=-115 rsrq=-20 rssnr=2147483647 cqi=2147483647 ta=2147483647 miuiLevel=2 level=1 android.telephony.CellConfigLte :{ isEndcAvailable = false }}, CellInfoLte:{mRegistered=NO mTimeStamp=244194682094009ns mCellConnectionStatus=0 CellIdentityLte:{ mBandwidth=2147483647 mMcc=null mMnc=null mAlphaLong= mAlphaShort=} CellSignalStrengthLte: rssi=-87 rsrp=-112 rsrq=-15 rssnr=2147483647 cqi=2147483647 ta=2147483647 miuiLevel=2 level=1 android.telephony.CellConfigLte :{ isEndcAvailable = false }}, CellInfoLte:{mRegistered=NO mTimeStamp=244194682094009ns mCellConnectionStatus=0 CellIdentityLte:{ mBandwidth=2147483647 mMcc=null mMnc=null mAlphaLong= mAlphaShort=} CellSignalStrengthLte: rssi=-87 rsrp=-112 rsrq=-17 rssnr=2147483647 cqi=2147483647 ta=2147483647 miuiLevel=2 level=1 android.telephony.CellConfigLte :{ isEndcAvailable = false }}";

  List<String> keys = ['rssi', 'rsrp', 'rsrq', 'rssnr', 'cqi', 'ta'];
  Map<String, dynamic> deviceInfo = {};

  getDeviceInfo() {
    var result;
    keys.forEach((e) {
      result = getKeyValuePair(needle: e, haystack: haystack);
      deviceInfo.addEntries({e: result}.entries);
    });

    setState(() {
      deviceInfo = deviceInfo;
    });
  }

  String getKeyValuePair(
      {@required String needle, @required String haystack, String edge = ' '}) {
    final startIndex = haystack.indexOf('$needle=');
    final endIndex = haystack.indexOf(edge, startIndex + needle.length);
    return haystack
        .substring(startIndex + needle.length, endIndex)
        .replaceAll(RegExp(r'='), '');
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ElevatedButton(
              child: Text('Get Device Info'),
              onPressed: getDeviceInfo,
            ),
            Text(deviceInfo.toString()),
          ],
        ),
      ),
    );
  }
}
